#!/bin/sh
#Script for certificates for Apache
#Made by Tim Hovius

echo "Generate a certificate for Apache.\nPlease fill the fields with proper information\n\n"

echo "Server Name (url of the server): "
read server
echo "E-mailadress: "
read email

openssl req -passout pass:abcdefg -subj "/C=NL/ST=Zuid-Holland/L=Gouda/O=ConnectSB/OU=Developer/CN=$server/emailAddress=$email" -new > $server.csr

openssl rsa -passin pass:abcdefg -in privkey.pem -out $server.key

openssl x509 -in $server.csr -out $server.crt -req -signkey $server.key -days 365

rm privkey.pem
rm $server.csr
