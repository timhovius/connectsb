#!/bin/bash

# install chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get update
sudo apt-get install -y google-chrome-stable

sudo apt-get install -y python-software-properties debconf-utils

# install openjdk 
sudo apt install -y openjdk-8-jdk

# install git
sudo apt-get -y install git-core

# install webserver
sudo apt install -y apache2
sudo apt install -y curl
sudo apt install -y php7.0
sudo apt install libapache2-mod-php7.0

# set password root and install mysql
echo "mysql-server mysql-server/root_password password root" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | sudo debconf-set-selections
sudo apt-get -y install mysql-server

# silent install phpmyadmin
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-user string root" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password root" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password root" |debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password root" | debconf-set-selections
sudo apt-get -y install phpmyadmin

# install php extensions
sudo apt install -y php-apc
sudo apt install -y php-mcrypt
sudo apt install -y php-mbstring
sudo apt install -y php-gettext
sudo apt install -y php7.0-mbstring
sudo apt install -y php7.0-xsl
sudo apt install -y php7.0-curl
sudo apt install -y php7.0-intl
sudo apt install -y php7.0-mysql
sudo apt install -y php7.0-sqlite3

# install composer globally
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# install node with less
# https://nodejs.org/en/download/package-manager/
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get -y install nodejs
sudo npm -g install less

# enable .htacces 
sudo a2enmod rewrite

# finaly, restart apache2
sudo service apache2 restart