#!/bin/sh
#Script for installing XDebug for Apache and PHP
#Made by Tim Hovius

git clone git://github.com/xdebug/xdebug.git

cd xdebug

phpize

./configure --enable-xdebug

make

cp modules/xdebug.so /usr/lib/php5

if ! grep --quiet xdebug /etc/php5/apache2/php.ini; then
	echo "\n" >> /etc/php5/apache2/php.ini
	echo "[xdebug]" >> /etc/php5/apache2/php.ini
	echo "zend_extension=\"/usr/lib/php5/xdebug.so\"" >> /etc/php5/apache2/php.ini

	# Important
	echo "xdebug.remote_enable=1" >> /etc/php5/apache2/php.ini
fi	

sudo service apache2 restart
cd ..
rm -rf xdebug
